---
layout: post
title: "Nahamcon 2022 Web Soruları - Easy"
tags: [ctf, nahamcon, web]
---

CTF Linki: [https://ctf.nahamcon.com/challenges](https://ctf.nahamcon.com/challenges)

**Kategori:** XML Injection, Regex Bypass, Recon

TLDR; Robots.txt den gelen flag. Regex bypasslanarak gelen flag. Son olarak da XML injection zafiyeti ile gelen flag.

Easy soruları es geçmek istemedim. Her biri ayrı yazılar olamayacak kadar kısa oldugu için de tek yazı altında toplamak istedim.

## Jurassic Park

![01](/assets/img/ctf/nahamcon2022/jurassicpark/banner_jurassicpark.png)

Uygulamaya giriş yapıldığında bizi aşağıdaki ekran karşılıyor.

![02](/assets/img/ctf/nahamcon2022/jurassicpark/jurassic-01.png)

Soru kolay kategorisinde olduğundan direkt olarak robots.txt ye yöneldim.

![03](/assets/img/ctf/nahamcon2022/jurassicpark/jurassic-02.png)

Robots.txt içindeki pathe gittiğimde ise flag geldi.

![04](/assets/img/ctf/nahamcon2022/jurassicpark/jurassic-03.png)

**FLAG:** `flag{c2145f65df7f5895822eb249e25028fa}`

## Personnel

![05](/assets/img/ctf/nahamcon2022/personnel/banner_personnel.png)

Bu uygulama her ne kadar kolay kategorisinde olsa da son 2 ye kalan sorularımdan biriydi. Uygulamaya giriş yaptığımızda arkada calışan bir veritabanının olmadığını söylüyor.

![06](/assets/img/ctf/nahamcon2022/personnel/personnel-01.png)

Lookup sorgusu attığımızda aşağıdaki gibi cevap geliyor.

![07](/assets/img/ctf/nahamcon2022/personnel/personnel-02.png)

Soru içerisinde paylaşılmış olan kaynak kodları incelediğimde iki dosyanın string concatenation ile birleştirildiğini gördüm.

![08](/assets/img/ctf/nahamcon2022/personnel/personnel-03.png)

Arama fonksiyonu da regex ile çalısıyordu. name parametresine gönderdiğimiz değer regex ifadenin içerisine yazılıyordu ve string içerisinde arama yapıyordu. `\n` karakterinden dolayı flagin olduğu satırı okuyamıyordum.

![09](/assets/img/ctf/nahamcon2022/personnel/personnel-04.png)

Uzuun araştırmalarım sonucunda `|` işareti ile regexi bypasslayabileceğimi keşfettim. İçerisindeki tüm özel karakterleri barındıran `[\S\n\t\v ]*|` payloadı gönderdim ve flag geldi.

![10](/assets/img/ctf/nahamcon2022/personnel/personnel-05.png)

**FLAG:** `flag{f0e659b45b507d8633065bbd2832c627}`

## EXtravagant

![11](/assets/img/ctf/nahamcon2022/extravagant/banner_extravagant.png)

Uygulamanın işlevi çok basit. XML al, işle ve kullanıcıya göster.

![12](/assets/img/ctf/nahamcon2022/extravagant/extravagant-01.png)

XML injection olabileceği için hızlıca aşağıdaki payloadı oluşturdum ve xml.xml adında bir dosyaya kaydettim.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE foo [ <!ENTITY xxe SYSTEM "file:///var/www/flag.txt"> ]>
<stockCheck><productId>&xxe;</productId></stockCheck>
```

Ardından uygulama üzerinden dosyayı gönderdim.

![13](/assets/img/ctf/nahamcon2022/extravagant/extravagant-02.png)

Yükleme başarılı.

![14](/assets/img/ctf/nahamcon2022/extravagant/extravagant-03.png)

View XML sekmesinden de göndermiş olduğum xml dosyasının adını yazdım.

![15](/assets/img/ctf/nahamcon2022/extravagant/extravagant-04.png)

Boom.

![16](/assets/img/ctf/nahamcon2022/extravagant/extravagant-05.png)

**FLAG:** `flag{639b72f2dd0017f454c44c3863c4e195}`
