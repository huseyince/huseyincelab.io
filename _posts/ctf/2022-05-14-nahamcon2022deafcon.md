---
layout: post
title: "Nahamcon 2022 Web Soruları - DeafCon"
tags: [ctf, nahamcon, web]
---

![](/assets/img/ctf/nahamcon2022/deafcon/banner_deafcon.png)

CTF Linki: [https://ctf.nahamcon.com/challenges](https://ctf.nahamcon.com/challenges)

**Kategori:** Server Side Template Injection (SSTI), Unicode Normalization

TLDR; Arka planda çalışan wkhtmltopdf v0.12.5 programında SSTI zafiyeti var. Bunun Unicode Normalization zafiyeti ile beraber sömürülmesi ve flagin okunması bekleniyor.

## Adimlar

CTF sırasında çözüme çok yaklaştığım fakat yanlış metodlar uyguladığım icin çözemediğim tek sorudur. Takıldığım yerlerden de bahsederek anlatmaya çalışacağım.

Uygulamaya giriş yaptığınızda sizi aşağıdaki ekran karşılıyor. İsim ve mail adresi girerek beleş biletimizi uygulamadan istiyoruz.

![01](/assets/img/ctf/nahamcon2022/deafcon/deafcon-01.png)

Uygulama bizim için bir bilet üretiyor. Buraya kadar her şey çok güzel.

![02](/assets/img/ctf/nahamcon2022/deafcon/deafcon-02.png)

Burada aklıma iki tane senaryo geliyor.

**Senaryo 1:** Arka planda çalışan uygulamada LFI zafiyeti var. Bu zafiyet kullanılarak sistemdeki dosyalar PDF üretim anında dosyaya dahil edilecek. Bu sayede flag okunmuş olacak.

**Senaryo 2:** PDF üreten uygulamanın bilindik bir zafiyeti var. Gönderdiğimiz payloadlar ile bunu tetikleyebiliyoruz. Bu sayede sistemde komut çalıştırarak flagi okuyabileceğiz.

Her iki senaryo için de ipuçları bulmaya çalıştım. İlk bulduğum ipucu aşağıdaki gibiydi. PDF'in wkhtmltopdf v0.12.5 ile üretildiğini gördüm.

![03](/assets/img/ctf/nahamcon2022/deafcon/deafcon-03.png)

İnternetten ufak bir arama ile [[1](https://www.virtuesecurity.com/kb/wkhtmltopdf-file-inclusion-vulnerability-2/)] uygulamanın LFI zafiyetine izin verdiğini gördüm. İkinci senaryoyu direkt olarak eledim. Çünkü uygulamada başka bilindik bir zafiyet yoktu. Direkt olarak payloadları denedim ama yanlış giden bir şeyler vardı.

İsim alanına iframe injection yapmaya çalıştığımda uygulama responseda aşağıdaki gibi bir cevap dönüyordu. Burada yapabileceğim pek bir şey olmadığını anladım ve mail alanına yöneldim.

```
The name can only contain [a-zA-Z0-9_] and spaces
```

Mail alanına özel karakterler ile oluşturulmuş bir payload gönderdiğimde ise aşağıdaki response u alıyordum. RFC dokumanını incelediğimde izin verilen ve işime yaraması muhtemel bazı karakterler buldum.

```
This email is not RFC5322-compliant
```

İzin verilen diğer karakterleri bulmak için isteği Burpten intrudera verdim ve sonuç aşağıdaki gibi. İşime yarayacak olan karakterlerin hepsi engelli gibiydi.

![04](/assets/img/ctf/nahamcon2022/deafcon/deafcon-04.png)

Biraz daha farklı payloadlar denedim ama sonuç alamadım. Iframe injection yapamayacağımı artık anladım ve aklıma bu sefer de farklı bir senaryo geldi. İzin verilen karakterler arasında { ve } vardı.

**Senaryo 3:** Arkada çalışan uygulama HTML i render ediyor. HTML i oluştururken Jinja, Twig vb bir template engine kullanıyor. SSTI zafiyeti ile komut çalıştırarak flag okunabilir.

Eldeki verilerle bu senaryo çok daha olasıydı. Uygulamada SSTI olup olmadığını tespit etmek icin `\{\{7*7}}` _(\ lar olmadan)_ payloadını gönderdim ve bana `49` cevabını döndü. SSTI olduğu aşikardı fakat uygulama normal parantezleri ve köşeli parantezleri kabul etmiyordu.

Biraz daha araştırırken Unicode Normalization saldırıları aklıma geldi. Küçük bir araştırma ile [[2](https://en.wikipedia.org/wiki/Halfwidth_and_Fullwidth_Forms_(Unicode_block))] unicode karakterlere ulaştım.

![05](/assets/img/ctf/nahamcon2022/deafcon/deafcon-05.png)

İsteği atarken, karakterleri kopyala yapıştır yapmak yerine karakterin unicode halini Burpten göndermeye çalıştım fakat encoded gondermem gerekiyormuş. Onu deneyimlemiş oldum.

> CTF süresi içerisinde soruyu buraya kadar getirebildim. Yazının devamı CTF sonrası.

Uygulamanın Jinja kullandığını tespit etmiştim. Aşağıdaki payloadı encode edip gönderdiğimizde ise flagi okuyabiliyoruz.

```jinja2
name=mnykmct&email=mnykmct@mail.com\{\{joiner.__init__.__globals__.os.popen（'cat${IFS}flag*'）.read（）}}
```

> Buradaki parantezler normal parantez değil, unicode parantezler. Ayrıca \ ların olmaması gerekiyor.

Uygulama unicode parantezleri görünce normal parantezler ile değiştiriyor ve işleme alıyor. Bu sayede zafiyeti sömürebiliyoruz.

![06](/assets/img/ctf/nahamcon2022/deafcon/deafcon-06.png)

**FLAG:** `flag{001a305ac5ab4b4ea995e5719ab10104}`

**Referanslar**
- [1] <https://www.virtuesecurity.com/kb/wkhtmltopdf-file-inclusion-vulnerability-2/>
- [2] <https://en.wikipedia.org/wiki/Halfwidth_and_Fullwidth_Forms_(Unicode_block)>
- <https://gist.github.com/ast3ro/ca6eec74293be5992f35b18023b420a4>
